from flask import Flask, jsonify, render_template, request, redirect
import requests
import uuid
import json
import time
app = Flask(__name__)

@app.route("/")
def hello():
    return redirect('/new-user')

@app.route("/new-event", methods=["GET", "OPTIONS", "POST"])
def new_event_view():
    saved = False
    error = None
    if request.method == "POST":
        r = requests.post("http://eventstore_write:5000/new", json={'type': request.form.get('type'),
                                                              'data': request.form.get('data'),
                                                              'id': request.form.get('uuid'),
                                                              'initiator': 'manual-test',
                                                              'streamid': request.form.get('streamid')})
        if r.status_code >= 200 and r.status_code < 300:
            saved = True
            time.sleep(0.1)
        else:
            error = r.text
    return render_template('new_event.html', saved=saved, error=error, uuid=str(uuid.uuid4()))

@app.route("/new-user", methods=["GET", "OPTIONS", "POST"])
def new_user_view():
    name = ""
    email = ""
    error = None
    if request.method == "POST":
        r = requests.post("http://eventstore_write:5000/new", json={'type': 'user:new',
                                                              'data': json.dumps({
                                                                  'name': request.form.get('name'),
                                                                  'email': request.form.get('email')
                                                              }),
                                                              'id': str(uuid.uuid4()),
                                                              'initiator': 'new-user',
                                                              'streamid': str(uuid.uuid4())})
        if r.status_code >= 200 and r.status_code < 300:
            time.sleep(0.1)
            return redirect('/users')
        else:
            name = request.form.get('name'),
            email = request.form.get('email')
            error = r.text or "Could not register user"
    return render_template('new_user.html', name=name, email=email, error=error, uuid=str(uuid.uuid4()))

@app.route("/users", methods=["GET", "OPTIONS"])
def users_view():
    r = requests.get("http://basic_data_api:3000/users")
    return render_template('user_list.html', users=r.json())

def render_user_view(id, err, saved, title, content):
    r = requests.get("http://basic_data_api:3000/user/" + id)
    r2 = requests.get("http://basic_data_api:3000/user/" + id + "/posts")
    user = r.json()
    posts = r2.json()
    name = user.get("name", "")
    email = user.get("email", "")
    return render_template(
        'user_page.html',
        userid=id,
        name=name,
        email=email,
        posts=posts,
        error=err,
        saved=saved,
        uuid=str(uuid.uuid4()),
        title=title,
        content=content
    )

@app.route("/user/<id>", methods=["GET", "OPTIONS"])
def user_view(id):
    return render_user_view(id, None, False, '', '')

@app.route("/post/<id>", methods=["GET", "OPTIONS"])
def post_view(id):
    r = requests.get("http://basic_data_api:3000/post/" + id)
    post = r.json()
    title = post.get('title', '')
    content = post.get('content', '')
    return render_template('post.html', title=title, content=content)

@app.route("/new-post/<userid>", methods=["POST"])
def new_post(userid):
    saved = False
    error = None
    title = ""
    content = ""
    r = requests.post("http://eventstore_write:5000/new", json={'type': 'posts:new',
                                                            'data': json.dumps({
                                                                  'title': request.form.get('title'),
                                                                  'content': request.form.get('content'),
                                                                  'id': request.form.get('uuid')
                                                              }),
                                                            'id': request.form.get('uuid'),
                                                            'initiator': 'user',
                                                            'streamid': userid})
    if r.status_code >= 200 and r.status_code < 300:
        saved = True
        time.sleep(0.1)
        return redirect("/user/" + userid)
    else:
        error = r.text or "Could not create post"
        title = request.form.get('title')
        content = request.form.get('content')
    return render_user_view(userid, error, saved, title, content)
