#!/usr/bin/env bash
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server

echo "create database projections;" | mysql -u root -proot
echo "create database \`event-db\`;" | mysql -u root -proot
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root';" | mysql -u root -proot
sudo cp /vagrant/db-setup/my.cnf /etc/mysql/mysql.conf.d/mysqld.cnf
sudo service mysql restart
