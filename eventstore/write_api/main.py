from flask import Flask, jsonify, request
import mysql.connector
app = Flask(__name__)

mydb = None
def get_mysql():
    global mydb
    if mydb == None:
        mydb = mysql.connector.connect(
            host="db",
            user="root",
            passwd="root",
            pool_name="eventpool",
            pool_size=10,
        )
    return mysql.connector.connect(pool_name="eventpool")

@app.route("/new", methods=["POST"])
def new_event():
    db = get_mysql()
    try:
        sql = "INSERT INTO events.events (id, type, data, streamid) VALUES (%s, %s, %s, %s);"
        vals = (request.json.get('id'), request.json.get('type'),
                request.json.get('data'), request.json.get('streamid'))
        cursor = db.cursor()
        cursor.execute(sql, vals)
        db.commit()
        print("Saved event", request.json.get('id'), "of type", request.json.get('type'), "for stream ",request.json.get('streamid'))
    except mysql.connector.IntegrityError as e:
        if e.errno == 1062:
            return jsonify({'success': True, 'duplicate': True})
        else:
            raise e
    finally:
        db.close()
    return jsonify({'success': True})
