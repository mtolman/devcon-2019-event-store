from flask import Flask, jsonify, request
import mysql.connector
import sys
app = Flask(__name__)

mydb = None
def get_mysql():
    global mydb
    if mydb == None:
        mydb = mysql.connector.connect(
            host="db",
            user="root",
            passwd="root",
            pool_name="eventpool",
            pool_size=10,
        )
    return mysql.connector.connect(pool_name="eventpool")

def db_row_to_json(row):
    return {'id': row[0],
            'type': row[1],
            'data': row[2],
            'timestamp': row[3],
            'streamid': row[4]}


@app.route("/first/", methods=["GET"])
@app.route("/first/<n>", methods=["GET"])
def first_n(n="10"):
    db = get_mysql()
    try:
        sql = "SELECT id, type, data, timestamp, streamid FROM events.events ORDER BY timestamp ASC LIMIT %s;"
        cursor = db.cursor()
        cursor.execute(sql, (int(n),))
        result = []
        for (id, type, data, timestamp, streamid) in cursor:
          result.append({'id': id, 'type': type, 'data': data, 'timestamp': timestamp, 'streamid': streamid})
        return jsonify(result)
    finally:
        db.close()


@app.route("/after/<id>/", methods=["GET"])
@app.route("/after/<id>/<n>", methods=["GET"])
def after_id(id, n="10"):
    db = get_mysql()
    try:
        sql = "SELECT id, type, data, timestamp, streamid FROM events.events WHERE timestamp > (SELECT timestamp FROM events.events WHERE id = %s) AND id <> %s ORDER BY timestamp ASC LIMIT %s;"
        cursor = db.cursor()
        cursor.execute(sql, (id, id, int(n)))
        result = []
        for (id, type, data, timestamp, streamid) in cursor:
          result.append({'id': id, 'type': type, 'data': data, 'timestamp': timestamp, 'streamid': streamid})
        return jsonify(result)
    finally:
        db.close()


@app.route("/<stream>/first/", methods=["GET"])
@app.route("/<stream>/first/<n>", methods=["GET"])
def stream_first_n(stream, n="10"):
    db = get_mysql()
    try:
        sql = "SELECT id, type, data, timestamp, streamid FROM events.events WHERE streamid = %s ORDER BY timestamp ASC LIMIT %s;"
        cursor = db.cursor()
        cursor.execute(sql, (stream, int(n)))
        result = []
        for (id, type, data, timestamp, streamid) in cursor:
          result.append({'id': id, 'type': type, 'data': data, 'timestamp': timestamp, 'streamid': streamid})
        return jsonify(result)
    finally:
        db.close()


@app.route("/<stream>/after/<id>/", methods=["GET"])
@app.route("/<stream>/after/<id>/<n>", methods=["GET"])
def stream_after_id(stream, id, n="10"):
    db = get_mysql()
    try:
        sql = "SELECT id, type, data, timestamp, streamid FROM events.events WHERE streamid = %s AND timestamp > (SELECT timestamp FROM events.events WHERE id = %s) AND id <> %s ORDER BY timestamp ASC LIMIT %s;"
        cursor = db.cursor()
        cursor.execute(sql, (stream, id, id, int(n)))
        result = []
        for (id, type, data, timestamp, streamid) in cursor:
          result.append({'id': id, 'type': type, 'data': data, 'timestamp': timestamp, 'streamid': streamid})
        return jsonify(result)
    finally:
        db.close()
