const MongoClient = require('mongodb').MongoClient;
const dbUrl = 'mongodb://db:27017/';
const dbName = 'basic_data';

const { linearRetry } = require('./utils');
let mongo = null;

// Create a new MongoClient
const client = new MongoClient(dbUrl, { useNewUrlParser: true });

const fetchById = collection => id =>
  linearRetry(
    () =>
      new Promise((resolve, reject) =>
        mongo
          .collection(collection)
          .findOne({ _id: id }, (err, item) =>
            err ? reject(err) : resolve(item)
          )
      )
  );

const connectToMongo = async () =>
  linearRetry(
    () =>
      new Promise((resolve, reject) =>
        client.connect(err => (err ? reject(err) : resolve(client)))
      )
        .then(client => client.db(dbName))
        .then(c => (mongo = c)),
    0,
    null
  );

const postLimit = 15;

const fetchPosts = filter => (page = 0) =>
  linearRetry(
    () =>
      new Promise((resolve, reject) =>
        mongo
          .collection('posts')
          .find(filter, { limit: postLimit, skip: page * postLimit })
          .toArray((err, data) => (err ? reject(err) : resolve(data)))
      )
  );

const fetchUsers = (page = 0) =>
  linearRetry(
    () =>
      new Promise((resolve, reject) =>
        mongo
          .collection('user')
          .find({}, { limit: postLimit, skip: page * postLimit })
          .toArray((err, data) => (err ? reject(err) : resolve(data)))
      )
  );

module.exports = {
  connect: connectToMongo,
  fetchUsers,
  fetchUser: fetchById('user'),
  fetchPost: fetchById('posts'),
  fetchPostsForUser: (userId, page = 0) => fetchPosts({ userId })(page),
  fetchRecommendedPosts: fetchPosts({})
};
