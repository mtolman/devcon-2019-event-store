const delay = (func, delayAmt) =>
  new Promise(resolve => setTimeout(() => resolve(func()), delayAmt));

const linearRetry = (func, attempts = 0, max = 4, ...args) =>
  delay(() => func(...args), attempts * 50).catch(err => {
    console.error(err)
    if (attempts < max || max === null)
      return linearRetry(func, attempts + 1, max, ...args);
    throw err;
  });

module.exports = {
  linearRetry,
  delay,
  groupByCollection: (acc = {}, cur = {}) => {
    acc[cur.collection] = acc[cur.collection] || [];
    acc[cur.collection].push(cur);
    return acc;
  }
};
