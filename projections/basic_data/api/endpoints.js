const mongo = require('./mongo');

const getUser = async ctx =>
  mongo.fetchUser(ctx.params.id).then(user => user ? ctx.body = user : ctx.status = 404);

const getPost = async ctx =>
  mongo.fetchPost(ctx.params.id).then(post => post ? ctx.body = post : ctx.status = 404);

const getUserPosts = async ctx =>
  mongo.fetchPostsForUser(ctx.params.id, ctx.params.page || 0).then(posts => {
    ctx.body = posts;
  });

const getRecommendedPosts = async ctx =>
  mongo.fetchRecommendedPosts(ctx.params.id, ctx.params.page || 0).then(posts => {
    ctx.body = posts;
  });

const getUsers = async ctx =>
  mongo.fetchUsers(ctx.params.page || 0).then(users => {
    ctx.body = users
  })

module.exports = { getUser, getUsers, getUserPosts, getRecommendedPosts, getPost };
