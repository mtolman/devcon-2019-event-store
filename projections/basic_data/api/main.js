const {getUser, getUsers, getUserPosts, getRecommendedPosts, getPost} = require('./endpoints');
const router = require('koa-router')();
const Koa = require('koa');
const app = new Koa();
const mongo = require('./mongo')

router
  .get('/users', getUsers)
  .get('/user/:id', getUser)
  .get('/user/:id/posts/:page?', getUserPosts)
  .get('/posts/:userid/:page?', getRecommendedPosts)
  .get('/post/:id', getPost);

app.use(router.routes());


mongo.connect().then(() => {
  console.log('Starting API on port 3000')
  app.listen(3000);
})
