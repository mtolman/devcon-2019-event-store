const fetch = require('node-fetch');
const { linearRetry } = require('./utils');
const eventServer = 'http://eventstore:5000';
const eventBatchSize = 50;

const requestUrl = lastEvent =>
  lastEvent
    ? `${eventServer}/after/${lastEvent}/${eventBatchSize}`
    : `${eventServer}/first/${eventBatchSize}`;

module.exports = {
  fetchEventBatch: lastEvent =>
    linearRetry(() => fetch(requestUrl(lastEvent)).then(res => res.json()))
};
