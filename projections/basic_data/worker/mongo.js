const MongoClient = require('mongodb').MongoClient;
const dbUrl = 'mongodb://db:27017/';
const dbName = 'basic_data';

const {linearRetry} = require('./utils')

// Create a new MongoClient
const client = new MongoClient(dbUrl, {useNewUrlParser: true});

module.exports = {
  connectToMongo: () =>
    linearRetry(() =>
      new Promise((resolve, reject) =>
        client.connect(err => (err ? reject(err) : resolve(client)))
      ).then(client => client.db(dbName))
    ),

  fetchLastEventId: db =>
    linearRetry(
      () =>
        new Promise((resolve, reject) =>
          db
            .collection('lastEvent')
            .findOne({ _id: 'lasteventprocessed' }, (err, item) =>
              err ? reject(err) : resolve(item ? item.eventid : null)
            )
        )
    ),

  updateLastProcessed: (db, last) => () =>
    linearRetry(
      () =>
        new Promise((resolve, reject) =>
          db
            .collection('lastEvent')
            .updateOne(
              { _id: 'lasteventprocessed' },
              { $set: { eventid: last.id } },
              { upsert: true },
              err => (err ? reject(err) : resolve(last.id))
            )
        )
    )
};
