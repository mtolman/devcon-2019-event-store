const delay = (func, delayAmt) =>
  new Promise(resolve => setTimeout(() => resolve(func()), delayAmt));

const linearRetry = (func, attempts = 0, ...args) =>
  delay(() => func(...args), Math.max(attempts * 100), 10000).catch(err => {
    console.error(err);
    return linearRetry(func, attempts + 1, ...args);
  });

module.exports = {
  linearRetry,
  delay,
  groupByCollection: (acc = {}, cur = {}) => {
    acc[cur.collection] = acc[cur.collection] || [];
    acc[cur.collection].push(cur);
    return acc;
  }
};
