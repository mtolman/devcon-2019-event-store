require('./event-processing')(eventToMongo).startProcessing();

const validCollections = ['user', 'posts'];
function eventToMongo({ type, id, data = '', streamid, timestamp }) {
  if (
    typeof id !== 'string' ||
    typeof streamid !== 'string' ||
    !(timestamp instanceof Date)
  )
    return null;

  const collection = type.substr(0, type.indexOf(':')).toLowerCase();
  if (validCollections.indexOf(collection) === -1) return null;

  try {
    data = JSON.parse(data);
  } catch {
    return null;
  }

  console.log(`Processing event ${id} of type ${type} for stream ${streamid}`);

  if (collection === 'posts') {
    data.userId = streamid;
    const _id = data.id;
    delete data.id;
    return {
      collection,
      filter: { _id },
      update: { $set: data }
    };
  } else {
    return {
      collection,
      filter: { _id: collection === 'user' ? streamid : data.id },
      update: { $set: data }
    };
  }
}
