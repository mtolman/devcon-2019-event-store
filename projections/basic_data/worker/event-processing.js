const emptyEventDelay = 25;

const {
  connectToMongo,
  fetchLastEventId,
  updateLastProcessed
} = require('./mongo');
const { delay, groupByCollection } = require('./utils');
const { fetchEventBatch } = require('./client');

const saveData = collection => ({ filter, update }) => {
  return new Promise((resolve, reject) =>
    collection.updateMany(filter, update, { upsert: true }, err =>
      err ? reject(err) : resolve()
    )
  );
};

module.exports = eventToMongo => {
  function processEvents({ db, events, lastId }) {
    if (!events.length) return Promise.resolve({ db, processed: 0, lastId });
    const last = events[events.length - 1];
    return Promise.all(
      Object.entries(
        events
          .map(e => ({ ...e, timestamp: new Date(e.timestamp) }))
          .map(eventToMongo)
          .filter(f => f)
          .reduce(groupByCollection, {})
      ).map(([collection, event]) =>
        Promise.all(event.map(saveData(db.collection(collection))))
      )
    )
      .then(updateLastProcessed(db, last))
      .then(newLastId => ({ db, processed: events.length, lastId: newLastId }))
      .catch(err => {
        console.error(err)
        return {db, processed: 0, lastId}
      })
  }

  function nextRequest(data) {
    return Promise.resolve(data)
      .then(({ db, processed, lastId }) =>
        processed
          ? fetchEventBatch(lastId).then(events => ({ db, events, lastId }))
          : delay(
              () =>
                fetchEventBatch(lastId).then(events => ({
                  db,
                  events,
                  lastId
                })),
              emptyEventDelay
            )
      )
      .then(processEvents)
      .then(nextRequest);
  }

  function startProcessing() {
    return delay(() => connectToMongo(), 3000)
      .then(db =>
        fetchLastEventId(db).then(lastId =>
          fetchEventBatch(lastId).then(events => ({ db, lastId, events }))
        )
      )
      .then(processEvents)
      .then(nextRequest);
  }
  return {
    processEvents,
    startProcessing
  };
};
