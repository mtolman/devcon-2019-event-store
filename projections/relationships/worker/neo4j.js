const neo4j = require('neo4j-driver').v1;

const driver = neo4j.driver(
  'bolt://db:7687',
  neo4j.auth.basic('neo4j', 'root')
);

module.exports = {
  connectToNeo4j = () => driver.session(),
  fetchLastEventId: db =>
    linearRetry(
      () => db
      .run('MATCH (e:LastEvent) RETURN e')
      .then(result => result.records[0].get(0))
    ),
  updateLastProcessed: (db, last) => () =>
    linearRetry(() => db.run('MERGE (e:LastEvent) SET e.lastId = $id RETURN e', {id: last.id})),
  saveData: (db, cmd, data) => linearRetry(() => db.run(cmd, data))
};