const emptyEventDelay = 50;

const {
  connectToNeo4j,
  fetchLastEventId,
  updateLastProcessed,
  saveData
} = require('./mongo');
const { delay, groupByCollection } = require('./utils');
const { fetchEventBatch } = require('./client');


module.exports = eventToNeo4j => {
  function processEvents({ db, events, lastId }) {
    if (!events.length) return Promise.resolve({ db, processed: 0, lastId });
    const last = events[events.length - 1];
    return Promise.all(
      Object.entries(
        events
          .map(e => ({ ...e, timestamp: new Date(e.timestamp) }))
          .map(eventToNeo4j)
          .filter(f => f)
      ).map((event) => Promise.all(event.map(saveData(db))))
    )
      .then(updateLastProcessed(db, last))
      .then(() => ({ db, processed: events.length, lastId: last.id }))
      .catch(() => ({db, processed: 0, lastId}))
  }

  function nextRequest(data) {
    return Promise.resolve(data)
      .then(({ db, processed, lastId }) =>
        processed
          ? fetchEventBatch(lastId).then(events => ({ db, events, lastId }))
          : delay(
              () =>
                fetchEventBatch(lastId).then(events => ({
                  db,
                  events,
                  lastId
                })),
              emptyEventDelay
            )
      )
      .then(processEvents)
      .then(nextRequest);
  }

  function startProcessing() {
    return delay(() => connectToMongo(), 3000)
      .then(db =>
        fetchLastEventId(db).then(lastId =>
          fetchEventBatch(lastId).then(events => ({ db, lastId, events }))
        )
      )
      .then(processEvents)
      .then(nextRequest);
  }
  return {
    processEvents,
    startProcessing
  };
};
